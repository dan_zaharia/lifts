package com.danz.controller;

import com.danz.model.Direction;
import com.danz.model.Lift;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Scheduler extends Thread {

    private Lift lift;
    private int timeout;
    private List<Boolean> destinations;

    public Scheduler(Lift lift, int timeout) {
        this.lift = lift;
        this.timeout = timeout;
        destinations = new ArrayList<>(Collections.nCopies(lift.getMAX_FLOOR() + 1, false));
    }

    public void goUp() throws  Exception {
        Thread.sleep(timeout);
        lift.setDirection(Direction.GOING_UP);
        lift.increaseFloor();
        System.out.println("Floor " + lift.getFloor());
    }

    public void goDown() throws  Exception {
        Thread.sleep(timeout);
        lift.setDirection(Direction.GOING_DOWN);
        lift.decreaseFloor();
        System.out.println("Floor " + lift.getFloor());
    }


    public void addDestination(int destination) throws  Exception {
        if (destination < 0 || destination > lift.getMAX_FLOOR()) {
            return;
        }
        destinations.set(destination, true);
        System.out.println(">>> Press button " + destination);
    }

    public boolean isCalled() {
        return destinations.stream().anyMatch(e -> e.equals(true));
    }

    private int findDestination() {
        return lift.getDirection() == Direction.GOING_DOWN ?
                destinations.indexOf(true) :
                destinations.lastIndexOf(true);
    }

    private boolean isDestination(int index) {
        return destinations.get(index);
    }

    private void reachDestination(int destination) {
        destinations.set(destination, false);
        System.out.println("Doors open at " + lift.getFloor());
    }


    public void run() {

        while (true) {

            try {
                if (isCalled()) {
                    int destination = findDestination();
                    while (destination < lift.getFloor()) {
                        goDown();
                        if (isDestination(lift.getFloor())) {
                            reachDestination(lift.getFloor());
                        }
                    }
                    while (destination > lift.getFloor()) {
                        goUp();
                        if (isDestination(lift.getFloor())) {
                            reachDestination(lift.getFloor());
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("!!!! " + e.getMessage());
            } finally {
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }
}
