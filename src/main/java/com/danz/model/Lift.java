package com.danz.model;

public class Lift {

    private int floor;
    private final int MAX_FLOOR;
    private Direction direction = Direction.STOPPED;

    public Lift(int maxFloor) {
        MAX_FLOOR = maxFloor;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public void increaseFloor() {
        this.floor++;
    }

    public void decreaseFloor() {
        this.floor--;
    }

    public int getMAX_FLOOR() {
        return MAX_FLOOR;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean isMoving() {
        return direction != Direction.STOPPED;
    }
}
