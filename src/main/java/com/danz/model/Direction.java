package com.danz.model;

public enum Direction {

    GOING_UP,
    GOING_DOWN,
    STOPPED;
}
